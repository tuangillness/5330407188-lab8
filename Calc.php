<?php
// file Calc.php
header("Content-type: text/xml");
$operation = $_GET['operation'];
$param1 = $_GET['param1'];
$param2 = $_GET['param2'];
if($operation=="add"){
$result = $param1+$param2;
} else if($operation=="subtract"){
$result = $param1-$param2;
} else {
echo "<?xml version=\"1.0\"?><error><usageInfo>You can use operation add
and subtract only. This service requires two parameters named param1 and param2. You may use
url Calc.php?operation=add&amp;param1=10&amp;param2=20</usageInfo></error>";
exit();
}
// Create root node of DOM tree
$doc = new DomDocument('1.0');
// Create root element of XML document
// This root element is calculator
$root = $doc->createElement('calculator');
// Set root element as a child of DOM tree
$root = $doc->appendChild($root);
// Create element node whose name is operation
$operationNode = $doc->createElement('operation');
// Set attribute where the first parameter is
// the attribute name and the second parameter
// is the value of attribute
if ($operation == "add") {
$operationNode->setAttribute('name','add');
} else if ($operation == "subtract") {
$operationNode->setAttribute('name', 'subtract');
}
$root->appendChild($operationNode);
$resultNode = $doc->createElement('result');
// Create text node
$resultValue = $doc->createTextNode($result);
// Set text node as a child of element node
$resultNode->appendChild($resultValue);
$operationNode->appendChild($resultNode);
// Save as xml string
$xml_string = $doc->saveXML();
// display the value of xml string
echo $xml_string;
?>